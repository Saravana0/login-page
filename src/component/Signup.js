import React from 'react';
import Background from '../component/images/Background.png';
import './Signup.css';
import { GoMail } from "react-icons/go";
import { AiOutlineUser, AiOutlineLock, AiOutlineEyeInvisible } from "react-icons/ai";


function Signup () {
    return (
        <div className='signup-page'>
            <div className='background-img'>
                <img src={Background} alt='background' /> 
                <p className='your-logo'> Your Logo </p>
            </div>
            <div className='signup-form'>
                <div className='sub-form'>
                    <p className='head-signup'> sign up </p>
                    <p className='decr-txt'> If you already have an account register </p>
                    <p className='login-here'> You can <span>Login here !</span> </p>
                    <div className='input-field'>
                        <p className='input-head'> Email </p>
                        <div className='email-address'>
                            <GoMail className='GoMail' />
                            <input type="text" id="email" name="email" placeholder="Enter your email address"/><br/>
                        </div>
                        <p className='input-head'> Username </p>
                        <div className='email-address2'>
                            <AiOutlineUser className='GoMail' />
                            <input type="text" id="email" name="email" placeholder="Enter your User name"/><br/>
                        </div>
                        <p className='input-head'> Password </p>
                        <div className='email-address2'>
                            <AiOutlineLock className='GoMail' />
                            <input type="text" id="email" name="email" placeholder="Enter your Password"/><br/>
                            <AiOutlineEyeInvisible className='eye' />
                        </div>
                        <p className='input-head'> Confrim Password </p>
                        <div className='email-address2'>
                            <AiOutlineLock className='GoMail' />
                            <input type="text" id="email" name="email" placeholder="confirm your password"/><br/>
                            <AiOutlineEyeInvisible className='eye' />
                        </div>
                    </div>
                    <button> Register </button>
                </div>
            </div>
        </div>
    );
}

export default Signup;